use std::{
    convert::Infallible,
    net::{IpAddr, SocketAddr},
    str::FromStr,
};

use reqwest::{Error as ReqwestError, Url};
use serde_json::json;
use thiserror::Error;
use tracing::{error, info, instrument, trace};
use warp::{http::StatusCode, Filter, Rejection, Reply};

#[derive(Error, Debug)]
enum HttpError {
    #[error("upstream failed: {0}")]
    UpstreamResponse(#[from] ReqwestError),
    #[error("no field in json: url: {0}, pointer: {1}")]
    Pointer(Url, String),
    #[error("invalid field in json: url: {0}, pointer: {1}")]
    Field(Url, String),
    #[error("invalid url: {0}")]
    Url(#[from] url::ParseError),
    #[error("bad escape sequences: {0}")]
    Quote(#[from] snailquote::UnescapeError),
}
impl warp::reject::Reject for HttpError {}

#[allow(clippy::upper_case_acronyms)]
#[derive(Debug)]
enum ReqMethod {
    GET,
    POST(serde_json::Value),
}

const POKEMON_START_URL: &str = "https://pokeapi.co/api/v2/pokemon/";
const SHAKESPEARE_URL: &str = "https://api.funtranslations.com/translate/shakespeare.json";

#[tokio::main]
async fn main() {
    use tracing_subscriber::FmtSubscriber;
    let subscriber = FmtSubscriber::builder()
        // all spans/events with a level higher than TRACE (e.g, debug, info, warn, etc.)
        // will be written to stdout.
        .with_max_level(tracing::Level::INFO)
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");

    let address = get_host_port();

    let pokepeare = warp::path("pokemon")
        .and(warp::path::param())
        .and_then(get_pokepeare)
        .recover(handle_rejection);

    info!("starting server");
    warp::serve(pokepeare).run(address).await;
}

/// Main request handler.
/// Produces a happy path response, or passes off error handling to
/// the rejection filters.
async fn get_pokepeare(name: String) -> Result<impl warp::Reply, Rejection> {
    Ok(warp::reply::json(&json!(
    {"name": name,
     "description": retrieve_pokemon_details(name).await?
        })))
}

/// Returns the Shakespearized Pokemon description based on the name
#[instrument]
async fn retrieve_pokemon_details(name: String) -> Result<String, HttpError> {
    use ReqMethod::*;
    let base_pokemon = Url::parse(POKEMON_START_URL)?;
    let details_url = json_ptr_from_url(GET, base_pokemon.join(&name)?, "/species/url").await?;

    trace!(%name, %details_url, "details url resolved");

    let story_text = json_ptr_from_url(
        GET,
        Url::parse(&details_url)?,
        "/flavor_text_entries/0/flavor_text",
    )
    .await?;

    trace!(%name, %story_text, "story text resolved");

    let shakespeare = json_ptr_from_url(
        POST(json!( {"text": snailquote::unescape(&story_text)? })),
        Url::parse(SHAKESPEARE_URL)?,
        "/contents/translated",
    )
    .await?;

    trace!(%name, %shakespeare, "shakespeare resolved");

    Ok(shakespeare)
}

/// Fetch a URL, parse the JSON response, and return with the value
/// pointed to by `ptr`, according to [serde_json::Value::pointer]
/// semantics
#[instrument]
async fn json_ptr_from_url(method: ReqMethod, url: Url, ptr: &str) -> Result<String, HttpError> {
    use ReqMethod::*;

    Ok(match method {
        GET => reqwest::get(url.clone()).await,
        POST(body) => {
            reqwest::Client::new()
                .post(url.clone())
                .json(&body)
                .send()
                .await
        }
    }?
    .error_for_status()?
    .json::<serde_json::Value>()
    .await?
    .pointer(ptr)
    .ok_or(HttpError::Pointer(url.clone(), ptr.into()))?
    .as_str()
    .ok_or(HttpError::Field(url.clone(), ptr.into()))?
    .into())
}

/// Parse command line arguments into the socket address where the
/// server binds
///
/// Defaults:
///
///  * `ip`: 127.0.0.1
///  * `port`: 3030
fn get_host_port() -> impl Into<SocketAddr> {
    use clap::{App, Arg};

    let args = App::new("Pokepear")
        .version("1.0")
        .author("PP")
        .about("Converts Pokemons to Hamlet")
        .arg(
            Arg::with_name("ip")
                .short("i")
                .long("ip")
                .help("IP address to bind")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("port")
                .short("p")
                .long("port")
                .help("Port to listen on")
                .takes_value(true),
        )
        .get_matches();

    let host = args
        .value_of("ip")
        .map(|i| IpAddr::from_str(i).expect("Invalid IP address format"))
        .unwrap_or([127, 0, 0, 1].into());

    let port = args
        .value_of("port")
        .map(|p| p.parse::<u16>().expect("Port not a number"))
        .unwrap_or(3030);

    (host, port)
}

/// Turn the internal error value into a user intelligible JSON response
async fn handle_rejection(err: Rejection) -> Result<impl Reply, Infallible> {
    let code;
    let message;

    if err.is_not_found() {
        code = StatusCode::NOT_FOUND;
        message = "Not found".to_string();
    } else if let Some(_) = err.find::<warp::reject::MethodNotAllowed>() {
        // We can handle a specific error, here METHOD_NOT_ALLOWED,
        // and render it however we want
        code = StatusCode::METHOD_NOT_ALLOWED;
        message = "Method not allowed".to_string();
    } else if let Some(e) = err.find::<HttpError>() {
        // This is an error coming from the Pokemon error chain, so we know we can serialize it properly
        error!(?e, "pokemon pipeline error");
        code = StatusCode::INTERNAL_SERVER_ERROR;
        message = format!("{}", e);
    } else {
        // We should have expected this... Just log and say its a 500
        error!(?err, "unhandled rejection");
        code = StatusCode::INTERNAL_SERVER_ERROR;
        message = "Unknown".to_string();
    }

    let json = warp::reply::json(&json!({ "error": message }));

    Ok(warp::reply::with_status(json, code))
}

#[cfg(test)]
mod test {
    #[test]
    fn check_pipeline() {
        let details =
            tokio_test::block_on(crate::retrieve_pokemon_details("bulbasaur".into())).unwrap();

        assert_eq!(
            details,
            "A strange seed wast planted on its back at birth. The plant sprouts and grows with this pokémon.".to_owned()
        )
    }
}
