FROM rust:alpine

COPY . /build/
WORKDIR /build/

cargo build --release

FROM alpine

COPY --from=build /build/target/release/pokepeare /pokepeare

CMD ["/pokepear"]
