Pokepeare
=========

Pokepeare is a microservice that retrieves some details about a
Pokemon in Shakespearean style.

The repository contains tests, Dockerfile, and everything else that
should get you going quickly.

## How to use

### Install the Rust toolchain
To ensure you have the latest Rust toolchain, install [Rustup](https://rustup.rs). 

If you run on macOS and you have [brew](https://brew.sh) installed,
you can go ahead and run the following commands to bootstrap your Rust
developer environment.

    brew rustup-init

Most Linux systems will also package Rustup.
On Alpine, run

    apk add rustup
    
On Arch, you'll need to run

    pacman -S rustup

On NixOS, you can use

    nix-env -iA nixpkgs.rustup
	
And your preferred distribution will also have this. After you get
Rustup up and running, run `rustup` or `rustup-init` to bootstrap the
toolchains.

If your package manager doesn't have Rustup, and you like living on
the edge, you can run the installer by piping curl into shell:

	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

The installer should lead you through the necessary steps.

### Run Pokepeare

You can run Pokepeare using the usual Rust commands:

	cargo run --release
	
The default settings will start the server at `localhost:3030`. This
is not always optimal, so you can bind the server to all IP addresses
on port 8000 like so:

	cargo run --release -- -i 0.0.0.0 -p 8000

For the full list of options, the `--help` option is your friend.

	cargo run --release -- --help
	
### Using the API

To get the Shakespearean description of your favourite Pokemon, you
can access the `/pokemon/<name>` endpoint using your favourite HTTP
client on the command line:

	$ curl localhost:3030/pokemon/bulbasaur

	{"description":"A strange seed wast planted on its back at birth. The plant sprouts and grows with this pokémon.","name":"bulbasaur"}

The `description` field will return the sylized response, while the `name` field is equal to the name of the beast you queried.

### Packaging

For packaging, a `Dockerfile` is provided. Note that this file is
untested, as I don't have access to a machine running Docker at the
moment. I lifed it from my private CI, so it should be fine.

`reqwest` uses [Rustls](https://crates.io/crates/rustls), a native
Rust TLS library as its backend, with an internal certificate store,
therefore the host OS should not need anything extra apart from libc
and the `pokepeare` binary.

## Testing

I provided an example end-to-end verification of the upstream handler
pipeline.

This method is useful to detect and pinpoint any error in the chain
due to the construction of the error management, but it does involve
an upstream query on each test run.

Running tests against external dependencies is not great practice,
because parallel test runs or other externalities can cause the tests
to fail erroneously, and provide false positive results.

To address this, including the upstream JSON responses in the
repository, and verifying that the JSON pointers behave correctly
would disconnect the parsing code path verification from the
connection handler verification.

While I don't condone mocking, a dummy API server returning the
aforementioned JSON responses can be a useful tool to completely
isolate the tests from external services.

However, the utility of all this effort is questionable to me until
the code receives high churn. The primary thing we want to notice when
it breaks _is_ in fact the upstream API contract.

For this reason, I would focus efforts on live healthchecks instead of
adding more code to such a focused service.

Until more testing effort is justified, I think the principled error
management, the type system guarantees, code review, and the tiny
codebase should be sufficient to protect against breakage during
change.

## Further improvements

 * Add more tracing backends.
 * Add local parser tests.

## License

Distributed under the MIT license.
